PKGNAME := exec-notify

.PHONY: all
all: install

.PHONY: install
install:  ## Install with pacman (on Arch Linux)
	sudo pacman -S --needed base-devel
	cd packaging && makepkg -fCcsri && rm -rf $(PKGNAME)

.PHONY: clean
clean:
	sudo pacman -Rns python-$(PKGNAME)-git
