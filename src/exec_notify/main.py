#!/usr/bin/env python3
import sys
# print(sys.path)
from sys import argv
import socket
from typing import List

from exec_notify.lib import exec, mail, util


def main():
    """
    Usage:
      exec-notify <command>

      with command = <executable> [<argument> ...]

    :return: exit code of executed command
    """

    if len(argv) <= 1:
        print('No command given to execute!', file=sys.stderr)
        exit(1)

    exit(execute_command(argv[1:]))


def execute_command(command: List[str]) -> int:
    """
    Executes the given command and sends an email on failure.

    :return: exit code of executed command
    """

    try:
        exit_code, stdout, stderr = exec.execute(command)
    except Exception as e:
        exit_code, stdout, stderr = 1, '', 'Caught subprocess exception: ' + str(e)

    prefix = '┃   '
    BODY = f'┏{"━" * 19}\n' \
           f'┣╸Command:\n' \
           f'{util.appendLinePrefix(prefix, str(command))}\n' \
           f'┣╸Exit Code:\n' \
           f'{util.appendLinePrefix(prefix, str(exit_code))}\n' \
           f'┣╸stderr:\n' \
           f'{util.appendLinePrefix(prefix, stderr)}\n' \
           f'┣╸stdout:\n' \
           f'{util.appendLinePrefix(prefix, stdout)}\n' \
           f'┗{"━" * 19}'
    print(BODY)

    if exit_code != 0:
        subject = f'{socket.gethostname()} | {str(command)}'
        mail.send_mail_or_write_to_file(subject=subject, body=BODY)

    return exit_code


if __name__ == '__main__':
    main()
