#!/usr/bin/env python3
import sys
# print(sys.path)
import socket

from exec_notify.lib import mail


def main():
    """
    Send an email with some content and optionally specify an email subject.

    Usage:
      echo <body> | do-notify

      echo <body> | do-notify <subject>

      do-notify <body>

      do-notify <subject> <body>
    """

    BODY = None
    subj = None
    hostname = socket.gethostname()

    if len(sys.argv) > 3:
        print('exec-notify>> Expected at most two arguments!', file=sys.stderr)
        exit(1)
    if len(sys.argv) == 2 or len(sys.argv) == 3:
        subj = sys.argv[1]
    if len(sys.argv) == 3:
        BODY = sys.argv[2]

    if subj is None:
        subj = 'notify'
    if BODY is None:
        BODY = sys.stdin.read()

    SUBJECT = f'{hostname} | {subj}'
    print(SUBJECT)
    print(BODY)

    mail.send_mail_or_write_to_file(subject=SUBJECT, body=BODY)


if __name__ == '__main__':
    main()
