import traceback
from sys import stderr
from pathlib import Path, PosixPath
import configparser
from typing import Tuple

from exec_notify.lib import util


class LazyConfig:
    # (static) class variable
    _config: configparser.ConfigParser = None

    @staticmethod
    def get(section: str, key: str):
        if LazyConfig._config is None:
            LazyConfig._config = configparser.ConfigParser()
            LazyConfig._config.read(_getCfgFile())
        return LazyConfig._config[section][key]


def getHostAndPort() -> Tuple[str, int]:
    try:
        return LazyConfig.get('mail', 'host'), int(LazyConfig.get('mail', 'port'))
    except Exception:
        print(f'exec-notify>> Could not read value [mail][host] from {_getCfgFile()}', file=stderr)
        traceback.print_exc()
        exit(1)


def getPassword() -> str:
    try:
        return LazyConfig.get('mail', 'password')
    except Exception:
        print(f'exec-notify>> Could not read value [mail][password] from {_getCfgFile()}', file=stderr)
        traceback.print_exc()
        exit(1)


def getFrom() -> str:
    try:
        return LazyConfig.get('mail', 'from')  # used for mail login as well
    except Exception:
        print(f'exec-notify>> Could not read value [mail][from] from {_getCfgFile()}', file=stderr)
        traceback.print_exc()
        exit(1)


def getTo() -> str:
    try:
        return LazyConfig.get('mail', 'to')
    except Exception:
        print(f'exec-notify>> Could not read value [mail][to] from {_getCfgFile()}', file=stderr)
        traceback.print_exc()
        exit(1)


def getMailDir() -> Path:
    try:
        return Path(LazyConfig.get('file', 'maildir'))
    except Exception:
        print(f'exec-notify>> Could not read value [file][maildir] from {_getCfgFile()}', file=stderr)
        traceback.print_exc()
        exit(1)


#
# Helper Methods
#


def _getCfgFile() -> Path:
    return _getCfgDir().joinpath('cfg.ini')


def _getCfgDir() -> Path:
    if util.isInDevelopment():
        return util.getProjectBase().joinpath('etc', 'exec-notify')
    else:
        return PosixPath('/etc/exec-notify/')
