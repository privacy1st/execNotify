import sys
from typing import List
from sys import stdin
import subprocess


def execute(command: List[str]):
    """
    Runs the given command in a subprocess and passes stdin (if given) to that process.
    Waits for command to complete.

    :param command: A command to executed as list of words, e.g. `['echo', 'Hello world!']`
    :return: (exit_code, stdout, stderr)
    :raises Exception: Might throw an exception.E.g. FileNotFoundError
     if the executable (first element of `command`) does not exist.
    """

    if sys.version_info.major == 3 and sys.version_info.minor < 7:
        # capture_output was added in 3.7
        # - https://docs.python.org/3/library/subprocess.html#subprocess.run

        completed: subprocess.CompletedProcess = subprocess.run(
            command,
            stdin=stdin,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=False,
        )
        return completed.returncode, str(completed.stdout), str(completed.stderr)

    completed: subprocess.CompletedProcess = subprocess.run(
        command,
        stdin=stdin,
        capture_output=True,
        check=False,
        text=True
    )
    return completed.returncode, completed.stdout, completed.stderr
