import sys
import datetime
import smtplib
import ssl

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from exec_notify.lib import config, util


def send_mail_or_write_to_file(subject: str, body: str, inform_about_local_mail: bool = True):
    if inform_about_local_mail and _local_mail_exists():
        mail_dir = config.getMailDir()

        subject = f'{subject} | UNREAD LOCAL MAIL'
        body = f'[!] Note [!]\n' \
               f'There is local mail inside {mail_dir} that was not delivered previously! ' \
               f'Please read and then delete it to get rid of this warning.\n\n\n' \
               f'{body}'

    try:
        send_mail(subject=subject, body=body)
    except Exception as e:
        print(f'exec-notify>> Could not send mail: {e}', file=sys.stderr)
        print(f'exec-notify>> Writing to file instead ...', file=sys.stderr)

        # Instead, try to save the mail so that the user can read
        # it later when connected to this computer
        save_mail(subject=subject, body=body)


def send_mail(subject: str, body: str):
    """
    :raises Exception: If mail could not be sent
    """

    from_ = config.getFrom()
    to = config.getTo()
    password = config.getPassword()

    # Create a secure SSL context
    context = ssl.create_default_context()

    host, port = config.getHostAndPort()
    with smtplib.SMTP_SSL(host=host, port=port, context=context) as server:
        server.login(from_, password)
        message = MIMEMultipart()
        message["Subject"] = subject
        message["From"] = from_
        message["To"] = to
        message["Bcc"] = to  # Recommended for mass emails
        # Turn plain/html message_body into plain/html MIMEText objects
        message.attach(MIMEText(body, "plain"))

        server.sendmail(from_, to, message.as_string())


def save_mail(subject: str, body: str):
    """
    This method does NOT throw exceptions
    """

    time = datetime.datetime.now()
    time_str = time.strftime('%Y%m%d_%H%M%S')

    mail_dir = config.getMailDir()
    mail_file = mail_dir.joinpath(time_str)

    prefix = '┃   '
    mail_str = f'┏{"━" * 19}\n' \
               f'┣╸Date:\n' \
               f'{util.appendLinePrefix(prefix, time_str)}\n' \
               f'┣╸Subject:\n' \
               f'{util.appendLinePrefix(prefix, subject)}\n' \
               f'┣╸Body:\n' \
               f'{util.appendLinePrefix(prefix, body)}\n' \
               f'┗{"━" * 19}'

    try:
        # create parent directory if not existent
        mail_dir.mkdir(parents=True, exist_ok=True)

        # append to file; create file if not existent
        with open(mail_file, "a") as f:
            f.write(mail_str)
    except Exception as e:
        print(f'exec-notify>> Could not write to file: {e}', file=sys.stderr)


def _local_mail_exists():
    """
    :return: True if local mail exists in maildir folder. Once the mail is read the user shall delete (or move) it.
    """

    mail_dir = config.getMailDir()
    if not mail_dir.exists():
        return False
    else:
        # mail_dir has at least one child (file/folder)
        return len(list(mail_dir.iterdir())) > 0
