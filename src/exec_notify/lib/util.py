import os
from pathlib import Path


def isInDevelopment() -> bool:
    """
    Helpful for local development where this python module is not installed.

    :returns: True if environment variable DE_P1ST_EXEC_NOTIFY is set.
    """
    return 'DE_P1ST_EXEC_NOTIFY' in os.environ


def getProjectBase() -> Path:
    return Path(os.path.realpath(__file__)).parent.parent.parent.parent.parent.parent


def readFirstLine(file: Path) -> str:
    """
    :param file: Path to file
    :returns: first line of file
    """
    with open(file, "r") as f:
        return f.readline()


def appendLinePrefix(prefix: str, s: str) -> str:
    """
    Append `prefix` to the left of each line in `s`

    :returns: modified `s`
    """

    if s is None or len(s) == 0:
        return prefix
    result = ''.join([prefix + line for line in s.splitlines(keepends=True)])
    if result.endswith(os.linesep):
        result += prefix
    return result
