import unittest

import exec_notify.lib.util as util


class MyTestCase(unittest.TestCase):
    def test1_appendLinePrefix(self):
        s = "This is a sentence.\nAnd this a second line."

        expected = "PREFIX This is a sentence.\nPREFIX And this a second line."
        actual = util.appendLinePrefix("PREFIX ", s)
        self.assertEqual(expected, actual)

    def test2_appendLinePrefix(self):
        s = "\n"

        expected = "PREFIX \nPREFIX "
        actual = util.appendLinePrefix("PREFIX ", s)
        self.assertEqual(expected, actual)

    def test3_appendLinePrefix(self):
        s = ""

        expected = "PREFIX "
        actual = util.appendLinePrefix("PREFIX ", s)
        self.assertEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
