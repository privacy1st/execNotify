# exec-notify

* Send email notification if command fails with [exec-notify](src/exec_notify/main.py).
* Send unconditional notifications with [do-notify](src/exec_notify/do_notify.py).

## TODOs

* ~~Send notification about unread local mail only once!~~
* ~~add PKGBUILD~~
* ~~Rename python module (and git repository?) to `exec-notify`~~
* Use subprocess_util > execute_print_capture
  * This way, the user can see the stdout/stderr while it is executed
  * And we can email the stdout/stderr if the process has failed

* New program flow:
  * Read configuration from `/etc/exec-notify` -> `work_dir` (default: `~/exec-notify`)
  * Create file `$work_dir` / `$start_date-$id.args` with `$args` as content, e.g. `ls foo bar`

  * If one or more .exit_code files exists and `$end_date` of at least one .exit_code file is older than 1 minute, then try to send email
    * If email was sent:
      * delete corresponding .exit_code, .stdout, .stderr, .args
      * continue
    * Else:
      * continue

  * Execute `$args` and redirect stdout/stderr to `$work_dir` / `$start_date-$id.{stdout,stderr}`
  * If error occurred, create file `$work_dir` / `$start_date-$id-$end-date.exit_code` with content `$exit_code`
    * Then, send email and if successful delete .exit_code file
  * Delete .args, .stdout, .stderr files
  * If error was reported, exit(1), else exit(0)

## Installation

### Installation - Arch Linux

```shell
make
```

### Installation - pip

Requires `pip` (e.g. `pacman -S --needed python-pip`).

Note: Make sure that pip is up-to-date and matches your python version:

```shell
python --version
#=> Python 3.10.1

pip --version
#=> pip 20.3.4 from /usr/lib/python3.10/site-packages/pip (python 3.10)
```

Then install with one of the following options:

```shell
sudo python3 -m pip install --upgrade --force-reinstall git+https://codeberg.org/privacy1st/exec-notify
sudo python3 -m pip install --upgrade --force-reinstall .
```

### Installation - venv

More detailed instructions about _venv_ can be found at [https://docs.python.org/3/library/venv.html](https://docs.python.org/3/library/venv.html)

Create a _venv_:

```shell
python3 -m venv ~/exec-notify/venv
```

Activate the venv:

```shell
source ~/exec-notify/venv/bin/activate
# echo "VIRTUAL_ENV=${VIRTUAL_ENV}"
```

And choose one source of installation:

```shell
# Install from git URL
python3 -m pip install git+https://codeberg.org/privacy1st/exec-notify

# Install from local directory
python3 -m pip install ~/Downloads/git/exec-notify/
```

(Optionally) list installed modules:

```shell
pip list
```

## Configuration

Create configuration file `/etc/exec-notify/cfg.ini`:

```shell
install -Dm0600 /etc/exec-notify/cfg.ini << 'EOF'
[DEFAULT]

[mail]
host = smtp.example.com
port = 465
password = examplePassword

from = noreply@example.com
to = me@example.com

[file]
maildir = /home/exampleUser/mail/
EOF
```

See also: [./etc/exec-notify/cfg.ini.example](etc/exec-notify/cfg.ini.example)


## Usage

### Usage of exec-notify

Add `exec-notify` in front of your command to execute.

**Example:**

`exec-notify ls /non/existent/file` will mail you the exit code, stdout and stderr of `ls /non/existent/file`

### Usage of notify

Send stdout via mail:

`echo "Hello world!" | do-notify`

Send stdout and stderr via mail:

`echo "Hello world!" 2>&1 | do-notify`

Send stdout and specify an optional email subject:

`echo "Hello world!" | do-notify "someSubject"`

Send message without using a pipe:

`do-notify "someSubject" "Hello World! What's up?"`


## Development

When started with environment variable `DE_P1ST_EXEC_NOTIFY` set,
then the configuration file is read from [./etc/exec-notify/cfg.ini](etc/exec-notify/cfg.ini).

### Testing

See heading "Installation" above with steps to locally install this module.

### Uploading to TestPyPI

More detailed instructions can be found at [https://packaging.python.org/tutorials/packaging-projects/](https://packaging.python.org/tutorials/packaging-projects/)

1) Set up a _venv_
2) Increase/Adjust `[metadata][version]` in [setup.cfg](setup.cfg)
3) Install the `build` and `twine` modules to your _venv_.
4) Next generate a [distribution archive](https://packaging.python.org/tutorials/packaging-projects/#generating-distribution-archives):

```shell
python3 -m build
```

5) Upload the distribution packages with twine. For the username, use `__token__`. For the password, use a 
[test.pypi.org API token](https://test.pypi.org/manage/account/#api-tokens):

```shell
python3 -m twine upload --repository testpypi dist/*
```

6) Congratulations! You can view the uploaded module under:

* [https://test.pypi.org/project/exec_notify/](https://test.pypi.org/project/exec_notify/)
